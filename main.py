from datetime import datetime

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"app_version": "v4", "current_datetime": datetime.now().isoformat()}

