#!/bin/bash
set -e

# This script must be executed as a part of "deploy.sh" script and similar scripts via `source deploy_setup.sh`.

# Configure Google Cloud.
gcloud config set project "$GCP_PROJECT_ID" --verbosity="info"
gcloud auth activate-service-account --key-file "$GCP_SERVICE_KEY"
gcloud config set compute/zone "$GCP_ZONE"
gcloud auth configure-docker eu.gcr.io,"$GCP_ZONE"-docker.pkg.dev --quiet --verbosity="info"
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
# PUSH to Google Cloud an existing Docker image that was built on previous stage.
CONTAINER_TAG=${CI_PIPELINE_ID}_${CI_COMMIT_SHORT_SHA} # Keep enclosed in brackets due to underscore.
docker pull "${CONTAINER_DEPLOY_IMAGE}"_"$CI_PIPELINE_ID"
export DEPLOY_IMAGE=$GCP_ZONE-docker.pkg.dev/$GCP_PROJECT_ID/$CI_PROJECT_NAME/$CI_PROJECT_NAME:$CONTAINER_TAG
docker tag "${CONTAINER_DEPLOY_IMAGE}"_"$CI_PIPELINE_ID" "${DEPLOY_IMAGE}"
docker push "${DEPLOY_IMAGE}"

# Delete stale images from Google Cloud Container Registry.
# gcloud artifacts docker images list $GCP_ZONE-docker.pkg.dev/$GCP_PROJECT_ID/$CI_PROJECT_NAME --format="value(DIGEST)" | tail -n +2 | while read -r digest; do
# 	gcloud artifacts docker images delete --verbosity="info" -q --delete-tags $GCP_ZONE-docker.pkg.dev/$GCP_PROJECT_ID/$CI_PROJECT_NAME/$CI_PROJECT_NAME@"${digest}"
# done

