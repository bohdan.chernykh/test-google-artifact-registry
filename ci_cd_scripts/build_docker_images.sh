#!/bin/bash
set -e

# Note: we write `CI_JOB_TOKEN` variable to file in order to use it as secret.
cd ..
CI_JOB_TOKEN_FILE_PATH="$PWD"/.ci_job_token
echo -n "${CI_JOB_TOKEN}" > "${CI_JOB_TOKEN_FILE_PATH}"

cd "${CI_PROJECT_DIR}"

# Final "deploy" stage
echo Building "${CONTAINER_DEPLOY_IMAGE}"_"${CI_PIPELINE_ID}"
docker pull "${CONTAINER_DEPLOY_IMAGE}"_latest || true
docker pull "${CONTAINER_DEPLOY_IMAGE_MAIN_BRANCH}" || true
docker pull "${CONTAINER_DEPLOY_IMAGE_DEV_BRANCH}" || true
docker build\
      -f ci_cd/Dockerfile\
      --pull\
      --build-arg BUILDKIT_INLINE_CACHE=1\
      --secret id=CI_JOB_TOKEN,src="${CI_JOB_TOKEN_FILE_PATH}"\
      -t "${CONTAINER_DEPLOY_IMAGE}"_"${CI_PIPELINE_ID}" .
docker push "${CONTAINER_DEPLOY_IMAGE}"_"${CI_PIPELINE_ID}" # PUSH to use in the next stages
docker tag "${CONTAINER_DEPLOY_IMAGE}"_"${CI_PIPELINE_ID}" "${CONTAINER_DEPLOY_IMAGE}"_latest
docker push "${CONTAINER_DEPLOY_IMAGE}"_latest
docker tag "${CONTAINER_DEPLOY_IMAGE}"_"${CI_PIPELINE_ID}" "${CONTAINER_DEPLOY_IMAGE_DEV_BRANCH}"
docker push "${CONTAINER_DEPLOY_IMAGE_DEV_BRANCH}"


